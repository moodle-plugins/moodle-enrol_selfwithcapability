<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * selfwithcapability enrolment plugin settings and presets.
 *
 * @package    enrol_selfwithcapability
 * @copyright  2022 Astor Bizard, 2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    // General settings.
    $settings->add(new admin_setting_heading('enrol_selfwithcapability_settings',
            '',
            get_string('pluginname_desc', 'enrol_selfwithcapability')));

    $settings->add(new admin_setting_configcheckbox('enrol_selfwithcapability/requirepassword',
            get_string('requirepassword', 'enrol_self'),
            get_string('requirepassword_desc', 'enrol_self'),
            0
            ));

    $settings->add(new admin_setting_configcheckbox('enrol_selfwithcapability/usepasswordpolicy',
            get_string('usepasswordpolicy', 'enrol_self'),
            get_string('usepasswordpolicy_desc', 'enrol_self'),
            0
            ));

    $settings->add(new admin_setting_configcheckbox('enrol_selfwithcapability/showhint',
            get_string('showhint', 'enrol_selfwithcapability'),
            get_string('showhint_desc', 'enrol_selfwithcapability'),
            0
            ));

    // Note: let's reuse the ext sync constants and strings here, internally it is very similar,
    // it describes what should happend when users are not supposed to be enerolled any more.
    $options = [
        ENROL_EXT_REMOVED_KEEP           => get_string('extremovedkeep', 'enrol'),
        ENROL_EXT_REMOVED_SUSPENDNOROLES => get_string('extremovedsuspendnoroles', 'enrol'),
        ENROL_EXT_REMOVED_UNENROL        => get_string('extremovedunenrol', 'enrol'),
    ];
    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/expiredaction',
            get_string('expiredaction', 'enrol_self'),
            get_string('expiredaction_help', 'enrol_self'),
            ENROL_EXT_REMOVED_KEEP,
            $options
            ));

    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/expirynotifyhour',
            get_string('expirynotifyhour', 'core_enrol'),
            '',
            6,
            range(0, 23)));

    // Enrol instance defaults.
    $settings->add(new admin_setting_heading('enrol_selfwithcapability_defaults',
            get_string('enrolinstancedefaults', 'admin'),
            get_string('enrolinstancedefaults_desc', 'admin')
            ));

    $settings->add(new admin_setting_configcheckbox('enrol_selfwithcapability/defaultenrol',
            get_string('defaultenrol', 'enrol'),
            get_string('defaultenrol_desc', 'enrol'),
            1
            ));

    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/status',
            get_string('status', 'enrol_selfwithcapability'),
            get_string('status_desc', 'enrol_selfwithcapability'),
            ENROL_INSTANCE_DISABLED,
            [
                    ENROL_INSTANCE_ENABLED  => get_string('yes'),
                    ENROL_INSTANCE_DISABLED => get_string('no'),
            ]));

    $yesno = [ 1 => get_string('yes'), 0 => get_string('no') ];
    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/newenrols',
            get_string('newenrols', 'enrol_self'),
            get_string('newenrols_desc', 'enrol_self'),
            1,
            $yesno
            ));

    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/groupkey',
            get_string('groupkey', 'enrol_self'),
            get_string('groupkey_desc', 'enrol_self'),
            0,
            $yesno
            ));

    if (!during_initial_install()) {
        $studentroles = get_archetype_roles('student');
        $settings->add(new admin_setting_configselect('enrol_selfwithcapability/roleid',
                get_string('defaultrole', 'enrol_selfwithcapability'),
                get_string('defaultrole_desc', 'enrol_selfwithcapability'),
                reset($studentroles)->id,
                get_default_enrol_roles(context_system::instance())
                ));
    }

    $settings->add(new admin_setting_configduration('enrol_selfwithcapability/enrolperiod',
            get_string('enrolperiod', 'enrol_self'),
            get_string('enrolperiod_desc', 'enrol_self'),
            0
            ));

    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/expirynotify',
            get_string('expirynotify', 'core_enrol'),
            get_string('expirynotify_help', 'core_enrol'),
            0,
            [
                    0 => get_string('no'),
                    1 => get_string('expirynotifyenroller', 'enrol_self'),
                    2 => get_string('expirynotifyall', 'enrol_self'),
            ]));

    $settings->add(new admin_setting_configduration('enrol_selfwithcapability/expirythreshold',
            get_string('expirythreshold', 'core_enrol'),
            get_string('expirythreshold_help', 'core_enrol'),
            86400,
            86400
            ));

    $options = [ 0 => get_string('never') ];
    foreach ([ 1800, 1000, 365, 180, 150, 120, 90, 60, 30, 21, 14, 7 ] as $ndays) {
        $options[$ndays * 86400] = get_string('numdays', '', $ndays);
    }
    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/longtimenosee',
            get_string('longtimenosee', 'enrol_self'),
            get_string('longtimenosee_help', 'enrol_self'),
            0,
            $options
            ));

    $settings->add(new admin_setting_configtext('enrol_selfwithcapability/maxenrolled',
            get_string('maxenrolled', 'enrol_self'),
            get_string('maxenrolled_help', 'enrol_self'),
            0,
            PARAM_INT
            ));

    $settings->add(new admin_setting_configselect('enrol_selfwithcapability/sendcoursewelcomemessage',
            get_string('sendcoursewelcomemessage', 'enrol_self'),
            get_string('sendcoursewelcomemessage_help', 'enrol_self'),
            ENROL_SEND_EMAIL_FROM_COURSE_CONTACT,
            enrol_send_welcome_email_options()));

    $settings->add(new admin_setting_configcheckbox('enrol_selfwithcapability/showunavailableenrolform',
            get_string('showunavailableenrolform', 'enrol_selfwithcapability'),
            get_string('showunavailableenrolform_desc', 'enrol_selfwithcapability'),
            1
            ));
}
